import { createStackNavigator } from 'react-navigation-stack'
import LoginScreen from '../containers/pages/Auth/Login'
import RegisterScreen from '../containers/pages/Auth/Register'

const AuthStack = createStackNavigator(
  {
    Login: LoginScreen,
    Register: RegisterScreen
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        height: 50
      }
    }
  }
)

export default AuthStack
