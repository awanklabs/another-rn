import { createStackNavigator } from 'react-navigation-stack'
import IntroductionScreen from '../containers/pages/Introduction'
import HomeScreen from '../containers/pages/Home'
import ProfileScreen from '../containers/pages/Profile'

import TopupStack from './TopupStack'
import EpaymentStack from './EpaymentStack'
import EpaymentTransactionStack from './EpaymentTransactionStack'

const AppStack = createStackNavigator(
  {
    Introduction: IntroductionScreen,
    Home: HomeScreen,
    Profile: ProfileScreen,
    Epayment: EpaymentStack,
    Topup: TopupStack,
    EpaymentTransaction: EpaymentTransactionStack
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
)

export default AppStack
