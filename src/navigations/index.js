import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import AppStack from './AppStack'
import AuthStack from './AuthStack'

const SwitchNavigator = createSwitchNavigator({
  // Login: LoginScreen,
  // Register: RegisterScreen,
  Auth: AuthStack,
  App: AppStack
})

export default createAppContainer(SwitchNavigator)
