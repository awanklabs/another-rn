import React from 'react'
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack'
import EpaymentTransactionScreen from '../containers/pages/EpaymentTransaction'
// Constants
import { theme } from '../configs/constants'

const EpaymentTransactionStack = createStackNavigator(
  {
    EpaymentTransaction: EpaymentTransactionScreen
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        fontSize: 16,
        color: `${theme.black}`,
        marginLeft: 0
      },
      headerLeft: (
        <HeaderBackButton
          tintColor={theme.primary}
          onPress={() => navigation.goBack(null)}
        />
      )
    })
  }
)

export default EpaymentTransactionStack
