import React from 'react'
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack'
import TopupScreen from '../containers/pages/Topup'
import TopupCreateScreen from '../containers/pages/Topup/TopupCreate'
// Constants
import { theme } from '../configs/constants'

const TopupStack = createStackNavigator(
  {
    Topup: TopupScreen,
    TopupCreate: TopupCreateScreen
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        fontSize: 16,
        color: `${theme.black}`,
        marginLeft: 0
      },
      headerLeft: (
        <HeaderBackButton
          tintColor={theme.primary}
          onPress={() => navigation.goBack(null)}
        />
      )
    })
  }
)

export default TopupStack
