import React from 'react'
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack'
import { createMaterialTopTabNavigator } from 'react-navigation-tabs'
import EpaymentScreen from '../containers/pages/Epayment'
import EpaymentPulsaScreen from '../containers/pages/Epayment/Pulsa'
import EpaymentPaketDataScreen from '../containers/pages/Epayment/PaketData'
import EpaymentTelponSmsScreen from '../containers/pages/Epayment/TelponSms'
import EpaymentPlnTokenScreen from '../containers/pages/Epayment/Pln/Token'
import EpaymentNonTokenPlnScreen from '../containers/pages/Epayment/Pln/NonToken'
import EpaymentInsuranceScreen from '../containers/pages/Epayment/Insurance'
import EpaymentFinanceScreen from '../containers/pages/Epayment/Finance'
import EpaymentPdamScreen from '../containers/pages/Epayment/Pdam'
import EpaymentGopayScreen from '../containers/pages/Epayment/Gopay'
import EpaymentGrabScreen from '../containers/pages/Epayment/Grab'
import EpaymentDigitalWalletScreen from '../containers/pages/Epayment/DigitalWallet'
import EpaymentTVScreen from '../containers/pages/Epayment/TV'
import EpaymentProviderScreen from '../containers/pages/Epayment/Provider'
import EpaymentGameScreen from '../containers/pages/Epayment/Game'
// Constants
import { theme } from '../configs/constants'

const EpaymentPln = createMaterialTopTabNavigator(
  {
    EpaymentNonTokenPln: EpaymentNonTokenPlnScreen,
    EpaymentPlnToken: EpaymentPlnTokenScreen
  },
  {
    navigationOptions: {
      title: 'PLN'
    }
  }
)

const EpaymentStack = createStackNavigator(
  {
    Epayment: EpaymentScreen,
    EpaymentPulsa: EpaymentPulsaScreen,
    EpaymentPaketData: EpaymentPaketDataScreen,
    EpaymentTelponSms: EpaymentTelponSmsScreen,
    EpaymentPln: EpaymentPln,
    EpaymentInsurance: EpaymentInsuranceScreen,
    EpaymentFinance: EpaymentFinanceScreen,
    EpaymentPdam: EpaymentPdamScreen,
    EpaymentGopay: EpaymentGopayScreen,
    EpaymentGrab: EpaymentGrabScreen,
    EpaymentDigitalWallet: EpaymentDigitalWalletScreen,
    EpaymentTV: EpaymentTVScreen,
    EpaymentProvider: EpaymentProviderScreen,
    EpaymentGame: EpaymentGameScreen
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitleStyle: {
        fontSize: 16,
        color: `${theme.black}`,
        marginLeft: 0
      },
      headerLeft: (
        <HeaderBackButton
          tintColor={theme.primary}
          onPress={() => navigation.navigate('Home')}
        />
      )
    })
  }
)

export default EpaymentStack
