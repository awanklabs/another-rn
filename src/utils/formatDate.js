import Moment from 'moment'

export default (date, format) => {
  return Moment(date).format(format)
}
