import CurrencyFormatter from 'currency-formatter'

export default value => {
  const result = CurrencyFormatter.format(Number(value), {
    symbol: 'Rp',
    decimal: ',',
    thousand: '.',
    precision: 0,
    format: '%s %v' // %s is the symbol and %v is the value
  })
  return result
}
