import formatPrice from './formatPrice'
import formatDate from './formatDate'

export const IS_DEV = process.env.NODE_ENV === 'development'

export const baseUrl = IS_DEV
  ? 'https://api.indonesiabright.com/v1'
  : 'https://api.indonesiabright.com/v1'

export { formatPrice, formatDate }

export * from './storage'
export * from './authFirebase'
