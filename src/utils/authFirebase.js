import firebase from '@react-native-firebase/app'
import '@react-native-firebase/messaging'
import AsyncStorage from '@react-native-community/async-storage'

export const checkPermission = async () => {
  const enabled = await firebase.messaging().hasPermission()
  if (enabled) {
    getToken()
  } else {
    requestPermission()
  }
}

//3
export const getToken = async () => {
  let fcmToken = await AsyncStorage.getItem('fcmToken')
  if (!fcmToken) {
    fcmToken = await firebase.messaging().getToken()
    if (fcmToken) {
      // user has a device token
      await AsyncStorage.setItem('fcmToken', fcmToken)
    }
  }
  // console.log(await AsyncStorage.getItem('fcmToken'))
}

//2
const requestPermission = async () => {
  try {
    await firebase.messaging().requestPermission()
    // User has authorised
    getToken()
  } catch (error) {
    // User has rejected permissions
    console.log('permission rejected')
  }
}
