import { combineReducers } from 'redux'
// Reducers
import globalReducer from './reducers/global'
import authReducer from './reducers/auth'
import topupReducer from './reducers/topup'
import epaymentReducer from './reducers/epayment'
import epaymentTransactionReducer from './reducers/epaymentTransaction'

const reducer = combineReducers({
  Global: globalReducer,
  Auth: authReducer,
  Topup: topupReducer,
  Epayment: epaymentReducer,
  EpaymentTransaction: epaymentTransactionReducer
})

export default reducer
