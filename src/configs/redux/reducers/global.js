import { globalConstant } from '../../constants'

const initialState = {
  author: 'awank <mas.awank100@gmail.com>',
  readIntro: false,
  toastMessage: null,
  isLoading: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case globalConstant.SET_TOAST_MESSAGE:
      return {
        ...state,
        toastMessage: action.payload
      }

    case globalConstant.SET_READ_INTRO:
      return {
        ...state,
        readIntro: action.payload
      }

    default:
      return state
  }
}
