import { epaymentTransactionConstant } from '../../constants'

const initialState = {
  page: 0,
  pages: null,
  limit: 10,
  sort: null,
  filter: {},
  data: [],
  activeId: null,
  detail: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case epaymentTransactionConstant.SET_EPAYMENT_TRANSACTION_PARAMS:
      return {
        ...state,
        page: action.payload.page ? action.payload.page : state.page,
        limit: action.payload.limit ? action.payload.limit : state.limit,
        sort: action.payload.sort ? action.payload.sort : state.sort,
        filter: action.payload.filter ? action.payload.filter : state.filter
      }

    case epaymentTransactionConstant.SET_EPAYMENT_TRANSACTION_FILTER:
      return {
        ...state,
        filter: action.payload
      }

    case epaymentTransactionConstant.SET_EPAYMENT_TRANSACTION_LIST:
      return {
        ...state,
        page: action.payload.page,
        pages: action.payload.pages,
        limit: action.payload.limit,
        data: action.payload.docs
      }

    case epaymentTransactionConstant.SET_EPAYMENT_TRANSACTION_ACTIVE_ID:
      return {
        ...state,
        activeId: action.payload
      }

    case epaymentTransactionConstant.SET_EPAYMENT_TRANSACTION_DETAIL:
      return {
        ...state,
        detail: action.payload
      }

    default:
      return state
  }
}
