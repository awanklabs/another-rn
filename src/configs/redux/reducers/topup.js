import { topupConstant } from '../../constants'

const initialState = {
  page: 0,
  pages: null,
  limit: 10,
  sort: null,
  filter: {},
  data: [],
  activeId: null,
  detail: null,
  listBank: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case topupConstant.SET_TOPUP_PARAMS:
      return {
        ...state,
        page: action.payload.page ? action.payload.page : state.page,
        limit: action.payload.limit ? action.payload.limit : state.limit,
        sort: action.payload.sort ? action.payload.sort : state.sort,
        filter: action.payload.filter ? action.payload.filter : state.filter
      }

    case topupConstant.SET_TOPUP_FILTER:
      return {
        ...state,
        filter: action.payload
      }

    case topupConstant.SET_TOPUP_LIST:
      return {
        ...state,
        page: action.payload.page,
        pages: action.payload.pages,
        limit: action.payload.limit,
        data: action.payload.docs
      }

    case topupConstant.SET_TOPUP_ACTIVE_ID:
      return {
        ...state,
        activeId: action.payload
      }

    case topupConstant.SET_TOPUP_DETAIL:
      return {
        ...state,
        detail: action.payload
      }

    case topupConstant.SET_TOPUP_DATA:
      return {
        ...state,
        data: action.payload
      }

    case topupConstant.SET_TOPUP_LIST_BANK:
      return {
        ...state,
        listBank: action.payload
      }

    default:
      return state
  }
}
