import { epaymentConstant } from '../../constants'

const initialState = {
  products: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case epaymentConstant.SET_EPAYMENT_PRODUCTS:
      return {
        ...state,
        products: action.payload
      }

    default:
      return state
  }
}
