import { authConstant } from '../../constants'
import api from '../../api'
import {
  readToken,
  saveToken,
  removeToken,
  readProfile,
  saveProfile,
  removeProfile,
  readFcmToken
} from '../../../utils'

export const login = args => async dispatch => {
  try {
    const url = '/login-app'
    const response = await api.post(url, args)

    if (response.status === 200) {
      await saveToken(response.data.data.Authorization)
      await dispatch(fetchProfile(false))
      await dispatch(setFire())
    }
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const setFire = () => async (dispatch, getState) => {
  try {
    const token = await readToken()
    const firebaseToken = await readFcmToken()
    const url = '/set-fire'
    await api.post(
      url,
      { firebaseToken },
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    )
  } catch (error) {
    return error.response.data
  }
}

export const register = args => async dispatch => {
  try {
    const url = '/register'
    const response = await api.post(url, args)

    if (response.status === 200) {
      await saveToken(response.data.data.Authorization)
      await dispatch(fetchProfile(false))
    }
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchProfile = (redirect = true) => async (dispatch, getState) => {
  try {
    const token = await readToken()
    const profile = await readProfile()

    if (redirect && token === null) {
      throw {
        status: 403,
        message: 'Anda belum login'
      }
    }

    if (token && profile === null) {
      const url = '/profile'
      const response = await api.get(url, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      await setProfile(response.data.data)
      dispatch({
        type: authConstant.SET_PROFILE,
        payload: response.data.data
      })
    }

    return {
      status: 200
    }
  } catch (error) {
    if (error.status === 403) {
      return error
    } else if (error.response.status === 400) {
      dispatch({
        type: authConstant.SET_PROFILE,
        payload: null
      })
      return {
        status: 403,
        message: error.response.data.message
      }
    }
    return error
  }
}

export const logout = () => async dispatch => {
  await removeToken()
  await dispatch(setProfile(null))
}

export const setProfile = profile => async dispatch => {
  dispatch({
    type: authConstant.SET_PROFILE,
    payload: profile
  })
  if (profile === null) {
    await removeProfile()
  } else {
    await saveProfile(profile)
  }
}
