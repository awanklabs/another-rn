import api from '../../api'
// Constants
import { topupConstant } from '../../constants'
// Utils
import { readToken } from '../../../utils'

const fetchData = () => async (dispatch, getState) => {
  try {
    const prefix = getState().Auth.prefix
    const state = getState().Topup
    const args = {
      limit: state.limit,
      page: state.page,
      sort: state.sort,
      filter: state.filter
    }
    const token = await readToken()

    const url = `/${prefix}/topups/list`
    const headers = {
      Authorization: `Bearer ${token}`
    }

    const response = await api.post(url, args, { headers })

    return response
  } catch (error) {
    return error.response.data
  }
}

export const fetchListBank = () => async (dispatch, getState) => {
  try {
    const prefix = getState().Auth.prefix

    const token = await readToken()
    const url = `/${prefix}/topups/allowed-bank`
    const headers = {
      Authorization: `Bearer ${token}`
    }

    const response = await api.get(url, { headers })
    if (response.status === 200) {
      const data = response.data.data
      console.log(data)
      dispatch({
        type: topupConstant.SET_TOPUP_LIST_BANK,
        payload: data
      })
    }
  } catch (error) {
    console.log(error)
    return error.response.data
  }
}

export const setParams = params => {
  return {
    type: topupConstant.SET_TOPUP_PARAMS,
    payload: params
  }
}

export const setFilter = filter => (dispatch, getState) => {
  const state = getState().Topup
  dispatch({
    type: topupConstant.SET_TOPUP_FILTER,
    payload: {
      ...state.filter,
      ...filter
    }
  })
}

export const fetchList = () => async (dispatch, getState) => {
  try {
    const response = await dispatch(fetchData())

    if (response.status === 200) {
      const data = response.data.data
      dispatch({
        type: topupConstant.SET_TOPUP_LIST,
        payload: data
      })
    }

    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchNextList = () => async (dispatch, getState) => {
  try {
    const state = getState().Topup
    const response = await dispatch(fetchData())

    if (response.status === 200) {
      const data = response.data.data
      if (data.docs.length > 0) {
        data.docs = state.data.concat(data.docs)
        dispatch({
          type: topupConstant.SET_TOPUP_LIST,
          payload: data
        })
      } else {
        dispatch(setParams({ page: state.page - 1 }))
      }
    }

    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchDetail = id => async (dispatch, getState) => {
  try {
    const prefix = getState().Auth.prefix
    const token = await readToken()

    const url = `/${prefix}/topups/${id}`
    const headers = {}
    if (token !== null) {
      headers.Authorization = `Bearer ${token}`
    }

    const response = await api.get(url, { headers })

    if (response.status === 200) {
      dispatch(setDetail(response.data.data))
    }
  } catch (error) {
    return error.response.data
  }
}

export const setActiveId = id => {
  return {
    type: topupConstant.SET_TOPUP_ACTIVE_ID,
    payload: id
  }
}

export const setDetail = detail => {
  return {
    type: topupConstant.SET_TOPUP_DETAIL,
    payload: detail
  }
}

export const createRequest = args => async (dispatch, getState) => {
  try {
    const prefix = getState().Auth.prefix
    const token = await readToken()

    const url = `/${prefix}/topups/create`
    const headers = { Authorization: `Bearer ${token}` }

    const response = await api.post(url, args, { headers })

    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const cancelRequest = id => async (dispatch, getState) => {
  try {
    const prefix = getState().Auth.prefix
    const token = await readToken()

    const url = `/${prefix}/topups/${id}/cancel`
    const headers = { Authorization: `Bearer ${token}` }

    const response = await api.get(url, { headers })

    return response.data
  } catch (error) {
    return error.response.data
  }
}
