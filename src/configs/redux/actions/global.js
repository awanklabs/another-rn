import { globalConstant } from '../../constants'

export const setToastMessage = message => {
  return {
    type: globalConstant.SET_TOAST_MESSAGE,
    payload: message
  }
}

export const setReadIntro = value => {
  return {
    type: globalConstant.SET_READ_INTRO,
    payload: value
  }
}
