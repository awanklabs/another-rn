import api from '../../api'
import { readToken } from '../../../utils'
import { epaymentConstant } from '../../constants'

export const fetchListProviders = type => async dispatch => {
  try {
    const token = await readToken()

    const url = `/member/epayments/list-providers/${type}`
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchProvider = prefix => async dispatch => {
  try {
    const token = await readToken()

    const url = `/member/epayments/provider/${prefix}`
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchProducts = (type, provider = '') => async dispatch => {
  try {
    const token = await readToken()

    let url = `/member/epayments/products/${type}`
    if (provider !== '') {
      url = `/member/epayments/products/${type}?provider=${provider}`
    }
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    dispatch({
      type: epaymentConstant.SET_EPAYMENT_PRODUCTS,
      payload: response.data.data
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchInquiry = args => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/inquiry'
    const response = await api.post(url, args, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchInquiryTokenPln = args => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/inquiry-token-pln'
    const response = await api.post(url, args, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const payPpob = args => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/pay'
    const response = await api.post(url, args, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchPlnToken = () => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/pln-token'
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchPlnNonToken = () => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/pln-non-token'
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchInsurance = () => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/insurance'
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchFinance = () => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/multifinance'
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchPdam = () => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/pdam'
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchTvPascabayar = () => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/tv-pascabayar'
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const fetchPpobProvider = () => async dispatch => {
  try {
    const token = await readToken()

    const url = '/member/epayments/ppob/provider'
    const response = await api.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}

export const buyProduct = args => async dispatch => {
  try {
    const token = await readToken()

    let url = '/member/epayments/products/buy'
    const response = await api.post(url, args, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    return response.data
  } catch (error) {
    return error.response.data
  }
}
