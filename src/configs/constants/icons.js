import React from 'react'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FeatherIcons from 'react-native-vector-icons/Feather'
import EntypoIcons from 'react-native-vector-icons/Entypo'
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome'
// Constants
import { theme } from '../../configs/constants'

export const IconPulsa = props => (
  <MaterialIcons
    name="phone-android"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconPaketData = props => (
  <MaterialIcons name="wifi" size={props.size || 30} color={theme.primary} />
)

export const IconTelpon = props => (
  <MaterialIcons name="phone" size={props.size || 30} color={theme.primary} />
)

export const IconPulsaPasca = props => (
  <MaterialIcons
    name="phonelink-ring"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconPLN = props => (
  <MaterialIcons
    name="flash-on"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconAsuransi = props => (
  <FeatherIcons name="shield" size={props.size || 30} color={theme.primary} />
)

export const IconAngsuran = props => (
  <MaterialCommunityIcons
    name="finance"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconPDAM = props => (
  <MaterialCommunityIcons
    name="water-pump"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconGasNegara = props => (
  <EntypoIcons name="air" size={props.size || 30} color={theme.primary} />
)

export const IconTV = props => (
  <MaterialCommunityIcons
    name="television"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconGojek = props => (
  <FontAwesomeIcons
    name="motorcycle"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconGrab = props => (
  <FontAwesomeIcons name="car" size={props.size || 30} color={theme.primary} />
)

export const IconGlobe = props => (
  <FontAwesomeIcons
    name="globe"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconMoney = props => (
  <FontAwesomeIcons
    name="money"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconGame = props => (
  <FontAwesomeIcons
    name="gamepad"
    size={props.size || 30}
    color={theme.primary}
  />
)

export const IconLainnya = props => (
  <MaterialCommunityIcons
    name="dots-horizontal"
    size={props.size || 30}
    color={theme.primary}
  />
)
