export default {
  SET_EPAYMENT_TRANSACTION_PARAMS: 'SET_EPAYMENT_TRANSACTION_PARAMS',
  SET_EPAYMENT_TRANSACTION_FILTER: 'SET_EPAYMENT_TRANSACTION_FILTER',
  SET_EPAYMENT_TRANSACTION_LIST: 'SET_EPAYMENT_TRANSACTION_LIST',
  SET_EPAYMENT_TRANSACTION_ACTIVE_ID: 'SET_EPAYMENT_TRANSACTION_ACTIVE_ID',
  SET_EPAYMENT_TRANSACTION_DETAIL: 'SET_EPAYMENT_TRANSACTION_DETAIL'
}
