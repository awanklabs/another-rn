export const theme = {
  background: '#f1f2f6',
  primary: '#2f80ed',
  primaryDark: '#2f59ed',
  black: '#4f4f4f',
  blackThin: '#b8b8b8',
  blackSuperThin: '#e2e2e2'
}
