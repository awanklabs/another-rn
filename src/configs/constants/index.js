import globalConstant from './global'
import authConstant from './auth'
import topupConstant from './topup'
import epaymentTypeConstant from './epaymentType'
import epaymentConstant from './epayment'
import epaymentTransactionConstant from './epaymentTransaction'

export const VERSION = ''

export * from './color'
export * from './icons'

export {
  globalConstant,
  authConstant,
  topupConstant,
  epaymentConstant,
  epaymentTypeConstant,
  epaymentTransactionConstant
}
