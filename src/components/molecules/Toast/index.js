import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Toaster from 'react-native-toaster'
// Actions
import { setToastMessage } from '../../../configs/redux/actions/global'

const Toast = () => {
  const dispatch = useDispatch()
  const toastMessage = useSelector(state => state.Global.toastMessage)

  return (
    <Toaster
      message={toastMessage}
      onHide={() => dispatch(setToastMessage(null))}
    />
  )
}

export default Toast
