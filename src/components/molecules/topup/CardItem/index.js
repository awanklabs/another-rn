import React from 'react'
import { ActivityIndicator } from 'react-native'
import styled from 'styled-components'
// Utils
import { formatPrice, formatDate } from '../../../../utils'
// Constants
import { theme } from '../../../../configs/constants'

const CardItem = props => {
  const { item, handleCancel, isLoading } = props
  return (
    <Container>
      <Header>
        <TextHeader>{item.invoice}</TextHeader>
        <TextHeader>
          {formatDate(item.requestDate, 'DD/MM/YYYY HH:mm')}
        </TextHeader>
      </Header>
      <Divider />
      <Content>
        <RowContent>
          <WrapperLabel>
            <Text>Bank</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <Text>{item.bankName}</Text>
          </WrapperValue>
        </RowContent>

        <RowContent>
          <WrapperLabel>
            <Text>Rekening</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <Text>{item.account}</Text>
          </WrapperValue>
        </RowContent>

        <RowContent>
          <WrapperLabel>
            <Text>Atas Nama</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <Text>{item.accountName}</Text>
          </WrapperValue>
        </RowContent>

        <RowContent>
          <WrapperLabel>
            <Text>Nominal Transfer</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <TextBold>{formatPrice(item.requestAmount)}</TextBold>
          </WrapperValue>
        </RowContent>

        <RowContent>
          <WrapperLabel>
            <Text>Batas Waktu</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <Text>{formatDate(item.expiredDate, 'DD/MM/YYYY HH:mm')} WIB</Text>
          </WrapperValue>
        </RowContent>

        <RowContent>
          <WrapperLabel>
            <Text>Status</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <TextStatus id={item.topupStatId._id}>
              {item.topupStatId.name}
            </TextStatus>
          </WrapperValue>
        </RowContent>

        {item.topupStatId._id === 1 && (
          <ButtonCancel onPress={() => handleCancel(item._id)}>
            {isLoading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <ButtonText>Batalkan</ButtonText>
            )}
          </ButtonCancel>
        )}
      </Content>
    </Container>
  )
}

export default CardItem

const Container = styled.View`
  background-color: white;
  border-width: 1px;
  border-color: ${theme.blackSuperThin};
  padding: 8px;
  border-radius: 8px;
  margin-bottom: 16px;
`

const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
`

const TextHeader = styled.Text`
  color: ${theme.black};
  font-size: 16px;
  font-weight: 500;
`

const Divider = styled.View`
  background-color: ${theme.blackSuperThin};
  height: 1px;
  margin-vertical: 4px;
`

const Content = styled.View``

const RowContent = styled.View`
  flex-direction: row;
`

const WrapperLabel = styled.View`
  flex: 1;
`

const WrapperValue = styled.View`
  flex: 2;
`

const Text = styled.Text`
  color: ${theme.black};
  font-size: 14px;
  font-weight: 500;
`

const TextBold = styled.Text`
  color: ${theme.black};
  font-size: 16px;
  font-weight: bold;
`

const TextStatus = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: ${props => {
    if (props.id === 1) {
      return 'orange'
    } else if (props.id === 3) {
      return 'blue'
    } else if (props.id === 4) {
      return 'red'
    }
  }};
`

const ButtonCancel = styled.TouchableOpacity`
  background-color: red;
  padding: 8px;
  align-items: center;
  margin-top: 8px;
  border-radius: 8px;
`

const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  text-align: center;
`
