import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import { theme } from '../../../configs/constants'

export default class RadioButtons extends Component {
  state = {
    value: null
  }

  render() {
    const { options } = this.props
    const { value } = this.state

    return (
      <View>
        {options.map(item => {
          return (
            <View key={item.key} style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.circle}
                onPress={() => {
                  this.setState({
                    value: item.key
                  })
                }}>
                {value === item.key && <View style={styles.checkedCircle} />}
              </TouchableOpacity>
              <Text>{item.text}</Text>
            </View>
          )
        })}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 30
  },

  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
    margin
  },

  checkedCircle: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: theme.primary
  }
})
