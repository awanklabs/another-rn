import React from 'react'
import styled from 'styled-components'
import AntDesignIcons from 'react-native-vector-icons/AntDesign'
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome'
// Constants
import { theme } from '../../../configs/constants'

const ProfileHeader = props => {
  return (
    <Container>
      <Navigation>
        <ButtonBack onPress={() => props.handleBack()}>
          <AntDesignIcons name="arrowleft" color="white" size={22} />
        </ButtonBack>
        <ButtonLogout onPress={() => props.handleLogout()}>
          <TextLogout>Keluar</TextLogout>
        </ButtonLogout>
      </Navigation>
      <HeaderContent>
        <FontAwesomeIcons name="user-circle" size={70} color="white" />
        <Name>{props.profile.name}</Name>
      </HeaderContent>
    </Container>
  )
}

export default ProfileHeader

const Container = styled.View`
  height: 200px;
  background-color: ${theme.primary};
`

const Navigation = styled.View`
  flex-direction: row;
  margin-top: 20px;
  margin-horizontal: 18px;
  align-items: center;
  justify-content: space-between;
`

const ButtonBack = styled.TouchableOpacity``

const ButtonLogout = styled.TouchableOpacity`
  border-width: 1px;
  border-color: white;
  padding: 8px;
  border-radius: 4px;
`

const TextLogout = styled.Text`
  font-size: 14px;
  color: white;
`

const HeaderContent = styled.View`
  flex: 1;
  align-items: center;
  /* align-content: center; */
  justify-content: center;
`

const Name = styled.Text`
  font-size: 30px;
  color: white;
`
