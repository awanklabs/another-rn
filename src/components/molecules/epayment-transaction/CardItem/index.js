import React from 'react'
import styled from 'styled-components'
// Utils
import { formatPrice, formatDate } from '../../../../utils'
// Constants
import { theme } from '../../../../configs/constants'

const CardItem = props => {
  const { item } = props
  return (
    <Container>
      <Header>
        <TextHeader>{item.invoice}</TextHeader>
        <TextHeader>
          {formatDate(item.createdAt, 'DD/MM/YYYY HH:mm')}
        </TextHeader>
      </Header>
      <Divider />
      <Content>
        <RowContent>
          <WrapperLabel>
            <Text>Transaksi</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <Text>
              {item.code} - {item.name}
            </Text>
          </WrapperValue>
        </RowContent>

        <RowContent>
          <WrapperLabel>
            <Text>Tujuan</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <Text>{item.destination}</Text>
          </WrapperValue>
        </RowContent>

        {item.ownerName && (
          <RowContent>
            <WrapperLabel>
              <Text>Atas Nama</Text>
            </WrapperLabel>
            <Text>: </Text>
            <WrapperValue>
              <Text>{item.ownerName}</Text>
            </WrapperValue>
          </RowContent>
        )}

        {item.period && (
          <RowContent>
            <WrapperLabel>
              <Text>Periode</Text>
            </WrapperLabel>
            <Text>: </Text>
            <WrapperValue>
              <Text>{item.period}</Text>
            </WrapperValue>
          </RowContent>
        )}

        {item.amount && (
          <RowContent>
            <WrapperLabel>
              <Text>Tagihan</Text>
            </WrapperLabel>
            <Text>: </Text>
            <WrapperValue>
              <Text>{formatPrice(item.amount)}</Text>
            </WrapperValue>
          </RowContent>
        )}

        {item.admin && (
          <RowContent>
            <WrapperLabel>
              <Text>Admin</Text>
            </WrapperLabel>
            <Text>: </Text>
            <WrapperValue>
              <Text>{formatPrice(item.admin)}</Text>
            </WrapperValue>
          </RowContent>
        )}

        {item.sellPrice && (
          <RowContent>
            <WrapperLabel>
              <Text>Harga</Text>
            </WrapperLabel>
            <Text>: </Text>
            <WrapperValue>
              <Text>{formatPrice(item.sellPrice)}</Text>
            </WrapperValue>
          </RowContent>
        )}

        <RowContent>
          <WrapperLabel>
            <Text>Respon</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <Text>{item.response}</Text>
          </WrapperValue>
        </RowContent>

        <RowContent>
          <WrapperLabel>
            <Text>Status</Text>
          </WrapperLabel>
          <Text>: </Text>
          <WrapperValue>
            <TextStatus id={item.epaymentTransactionStatId._id}>
              {item.epaymentTransactionStatId.name}
            </TextStatus>
          </WrapperValue>
        </RowContent>
      </Content>
    </Container>
  )
}

export default CardItem

const Container = styled.View`
  background-color: white;
  border-width: 1px;
  border-color: ${theme.blackSuperThin};
  padding: 8px;
  border-radius: 8px;
  margin-bottom: 16px;
`

const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
`

const TextHeader = styled.Text`
  color: ${theme.black};
  font-size: 16px;
  font-weight: 500;
`

const Divider = styled.View`
  background-color: ${theme.blackSuperThin};
  height: 1px;
  margin-vertical: 4px;
`

const Content = styled.View``

const RowContent = styled.View`
  flex-direction: row;
`

const WrapperLabel = styled.View`
  flex: 1;
`

const WrapperValue = styled.View`
  flex: 2;
`

const Text = styled.Text`
  color: ${theme.black};
  font-size: 14px;
  font-weight: 500;
`

const TextStatus = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: ${props => {
    if (props.id === 1) {
      return 'orange'
    } else if (props.id === 2) {
      return 'blue'
    } else if (props.id === 3 || props.id === 4) {
      return 'red'
    }
  }};
`
