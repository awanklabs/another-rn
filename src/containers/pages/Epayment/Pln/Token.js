import React, { Component } from 'react'
import styled from 'styled-components'
import { Picker, ActivityIndicator, Text } from 'react-native'
import { connect } from 'react-redux'
import AntDesignIcons from 'react-native-vector-icons/AntDesign'
import { selectContactPhone } from 'react-native-select-contact'
import { Formik } from 'formik'
import { ToastStyles } from 'react-native-toaster'
// Constants
import { theme } from '../../../../configs/constants'
// Actions
import {
  fetchInquiryTokenPln,
  fetchPlnToken,
  payPpob
} from '../../../../configs/redux/actions/epayment'
import { setToastMessage } from '../../../../configs/redux/actions/global'
// Utils
import { formatPrice } from '../../../../utils'
// Components
import Toast from '../../../../components/molecules/Toast'

class EpaymentPlnTokenScreen extends Component {
  static navigationOptions = {
    title: 'Token'
  }

  constructor(props) {
    super(props)
    this.state = {
      products: [],
      selectedProduct: null,
      detailInquiry: null,
      isLoading: false
    }
  }

  async componentDidMount() {
    await this.getProducts()
  }

  getProducts = async () => {
    this.setState({ isLoading: true, products: [], selectedProduct: null })
    const result = await this.props.dispatch(fetchPlnToken())
    if (result.status) {
      this.setState({ products: result.data })
      if (result.data.length > 0) {
        this.setState({ selectedProduct: result.data[0] })
      }
    }
    this.setState({ isLoading: false })
  }

  render() {
    const { isLoading, products, selectedProduct, detailInquiry } = this.state

    return (
      <Container>
        <Toast />
        <Formik
          enableReinitialize
          initialValues={{
            code: '',
            destination: ''
          }}
          onSubmit={async (values, { setErrors, setSubmitting }) => {
            setSubmitting(true)
            const args = {
              id: detailInquiry._id
            }
            const result = await this.props.dispatch(payPpob(args))
            if (result.status) {
              this.props.dispatch(
                setToastMessage({
                  text: result.message,
                  styles: ToastStyles.success
                })
              )
              this.props.navigation.navigate('Home')
            } else {
              result.errors && setErrors(result.errors)
              this.props.dispatch(
                setToastMessage({
                  text: result.message,
                  styles: ToastStyles.error
                })
              )
            }
            setSubmitting(false)
          }}>
          {({
            values,
            errors,
            handleSubmit,
            isSubmitting,
            setFieldValue,
            setErrors
          }) => {
            const getPhoneNumber = () => {
              return selectContactPhone().then(selection => {
                if (!selection) {
                  return null
                }
                let { selectedPhone } = selection
                let destination = selectedPhone.number
                destination = destination.replace(/\s/g, '')
                destination = destination.replace('+', '')
                destination = destination.replace(/-/g, '')
                if (destination.substring(0, 2) === '62') {
                  destination = destination.substring(2)
                  destination = `0${destination}`
                }
                setFieldValue('destination', destination)
                this.setState({ prefix: destination.substring(0, 4) })
                return selectedPhone.number
              })
            }

            const handleChangeDestination = value => {
              setFieldValue('destination', value)
              if (value.length < 4 && this.state.prefix !== '') {
                this.setState({ prefix: '' })
              } else if (
                value.length >= 4 &&
                value.substring(0, 4) !== this.state.prefix
              ) {
                this.setState({ prefix: value.substring(0, 4) })
              }
            }

            const handleChangeSelectedProduct = data => {
              this.setState({ detailInquiry: null, selectedProduct: data })
            }

            const handleInquiry = async () => {
              // this.props.dispatch(
              //   setToastMessage({
              //     text: 'Fitur ini dalam pengembangan',
              //     styles: ToastStyles.error
              //   })
              // )
              this.setState({ isLoading: true, detailInquiry: null })
              const args = {
                code: selectedProduct.code,
                destination: values.destination
              }
              const result = await this.props.dispatch(
                fetchInquiryTokenPln(args)
              )
              console.log(result)
              if (result.status) {
                if (result.data.status === 'Complete') {
                  this.setState({ detailInquiry: result.data })
                } else {
                  this.props.dispatch(
                    setToastMessage({
                      text: 'Coba Lagi',
                      styles: ToastStyles.error
                    })
                  )
                }
              } else {
                result.errors && setErrors(result.errors)
                this.props.dispatch(
                  setToastMessage({
                    text: result.message,
                    styles: ToastStyles.error
                  })
                )
              }
              this.setState({ isLoading: false })
            }

            const handleReset = () => {
              this.setState({ detailInquiry: null })
            }

            return (
              <>
                {products.length > 0 && (
                  <PickerWrapper>
                    <Picker
                      prompt="Daftar produk"
                      selectedValue={selectedProduct}
                      style={{
                        height: 50,
                        width: '100%'
                      }}
                      onValueChange={handleChangeSelectedProduct}>
                      <Picker.Item label="Silahkan pilih ..." value={null} />
                      {products.map((dt, index) => {
                        return (
                          <Picker.Item key={index} label={dt.name} value={dt} />
                        )
                      })}
                    </Picker>
                  </PickerWrapper>
                )}

                <Row style={{ marginTop: 10 }}>
                  <Input
                    placeholder="Masukkan ID PLN"
                    onChangeText={handleChangeDestination}
                    value={values.destination}
                    keyboardType="numeric"
                    hasError={errors.destination}
                  />
                  <ButtonContact onPress={getPhoneNumber}>
                    <AntDesignIcons name="contacts" color="white" size={20} />
                  </ButtonContact>
                </Row>
                <ErrorsView>
                  {errors.destination &&
                    errors.destination.map((error, index) => {
                      return (
                        <ErrorText key={index} numberOfLines={2}>
                          {error}
                        </ErrorText>
                      )
                    })}
                </ErrorsView>

                {isLoading && (
                  <ActivityIndicator
                    size="large"
                    color={theme.primary}
                    style={{ marginTop: 10 }}
                  />
                )}

                {detailInquiry === null && (
                  <ButtonInquiry
                    disabled={selectedProduct === null}
                    onPress={handleInquiry}>
                    {isLoading ? (
                      <ActivityIndicator size="small" color="white" />
                    ) : (
                      <Text
                        style={{
                          color: 'white',
                          textAlign: 'center',
                          fontSize: 20
                        }}>
                        Cek
                      </Text>
                    )}
                  </ButtonInquiry>
                )}

                {detailInquiry && (
                  <>
                    <ButtonReset onPress={handleReset}>
                      {isLoading ? (
                        <ActivityIndicator size="small" color="white" />
                      ) : (
                        <Text
                          style={{
                            color: 'white',
                            textAlign: 'center',
                            fontSize: 20
                          }}>
                          Reset
                        </Text>
                      )}
                    </ButtonReset>
                    <DetailCard>
                      <Row>
                        <WrapperLabel>
                          <Text>Produk</Text>
                        </WrapperLabel>
                        <Text>: </Text>
                        <WrapperValue>
                          <Text>{detailInquiry.name}</Text>
                        </WrapperValue>
                      </Row>
                      <Row>
                        <WrapperLabel>
                          <Text>Response</Text>
                        </WrapperLabel>
                        <Text>: </Text>
                        <WrapperValue>
                          <Text>{detailInquiry.response}</Text>
                        </WrapperValue>
                      </Row>
                      <Row>
                        <WrapperLabel>
                          <Text>Harga</Text>
                        </WrapperLabel>
                        <Text>: </Text>
                        <WrapperValue>
                          <Text>{formatPrice(detailInquiry.amount)}</Text>
                        </WrapperValue>
                      </Row>
                    </DetailCard>

                    <ButtonBuy disabled={detailInquiry === null}>
                      {isSubmitting ? (
                        <ActivityIndicator size="small" color="white" />
                      ) : (
                        <Text
                          style={{
                            color: 'white',
                            textAlign: 'center',
                            fontSize: 20
                          }}>
                          Bayar
                        </Text>
                      )}
                    </ButtonBuy>
                  </>
                )}
              </>
            )
          }}
        </Formik>
      </Container>
    )
  }
}

const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(
  null,
  mapDispatchToProps
)(EpaymentPlnTokenScreen)

const Container = styled.View`
  flex: 1;
  margin: 20px;
`

const Row = styled.View`
  flex-direction: row;
`

const Input = styled.TextInput`
  flex: 1;
  margin-right: 20px;
  padding-bottom: 0px;
  border-bottom-width: 1px;
  border-bottom-color: ${props =>
    props.hasError ? 'red' : `${theme.blackThin}`};
`

const ButtonContact = styled.TouchableOpacity`
  background-color: ${theme.primary};
  padding: 6px;
  border-radius: 4px;
  align-self: center;
`

const PickerWrapper = styled.View`
  border-bottom-width: 1px;
  border-color: ${theme.blackThin};
`

const DetailCard = styled.View`
  margin-top: 20px;
  border-width: 1px;
  border-color: ${theme.primary};
  padding: 10px;
  border-radius: 4px;
`

const WrapperLabel = styled.View`
  flex: 1;
`

const WrapperValue = styled.View`
  flex: 2;
`

const ButtonInquiry = styled.TouchableOpacity`
  margin-top: 20px;
  background-color: ${props => (props.detailInquiry ? 'red' : theme.primary)};
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  padding: 10px;
  border-radius: 4px;
`

const ButtonReset = styled.TouchableOpacity`
  margin-top: 20px;
  background-color: red;
  padding: 10px;
  border-radius: 4px;
`

const ButtonBuy = styled.TouchableOpacity`
  margin-top: 20px;
  background-color: ${theme.primary};
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  padding: 10px;
  border-radius: 4px;
`

const ErrorsView = styled.View``

const ErrorText = styled.Text`
  color: red;
`
