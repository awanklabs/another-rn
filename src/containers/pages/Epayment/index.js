import React, { Component } from 'react'
import styled from 'styled-components'
// Constants
import {
  IconPulsa,
  IconTelpon,
  IconPaketData,
  IconPLN,
  IconAngsuran,
  IconAsuransi,
  IconPDAM,
  IconTV,
  IconMoney,
  IconGojek,
  IconGrab,
  IconGlobe,
  IconGame,
  theme
} from '../../../configs/constants'

class EpaymentScreen extends Component {
  static navigationOptions = {
    title: 'Menu E-Payment'
  }

  render() {
    return (
      <Container>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentPulsa')}>
            <IconPulsa size={40} />
          </IconButton>
          <Title>Pulsa</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentPaketData')}>
            <IconPaketData size={40} />
          </IconButton>
          <Title>Paket Data</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentTelponSms')}>
            <IconTelpon size={40} />
          </IconButton>
          <Title>Pascabayar</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentPln')}>
            <IconPLN size={40} />
          </IconButton>
          <Title>PLN</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentInsurance')}>
            <IconAsuransi size={40} />
          </IconButton>
          <Title>Asuransi</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentFinance')}>
            <IconAngsuran size={40} />
          </IconButton>
          <Title>Finance</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentPdam')}>
            <IconPDAM size={40} />
          </IconButton>
          <Title>PDAM</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentGopay')}>
            <IconGojek size={40} />
          </IconButton>
          <Title>GO-PAY</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentGrab')}>
            <IconGrab size={40} />
          </IconButton>
          <Title>Grab</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() =>
              this.props.navigation.replace('EpaymentDigitalWallet')
            }>
            <IconMoney size={40} />
          </IconButton>
          <Title>Digital Wallet</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentTV')}>
            <IconTV size={40} />
          </IconButton>
          <Title>TV Langganan</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentGame')}>
            <IconGame size={40} />
          </IconButton>
          <Title>Game Online</Title>
        </EpyamentItem>
        <EpyamentItem>
          <IconButton
            onPress={() => this.props.navigation.replace('EpaymentProvider')}>
            <IconGlobe size={40} />
          </IconButton>
          <Title>Provider</Title>
        </EpyamentItem>
      </Container>
    )
  }
}

export default EpaymentScreen

const Container = styled.View`
  flex: 1;
  margin: 20px;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`

const EpyamentItem = styled.View`
  margin-bottom: 14px;
  width: 30%;
  justify-content: center;
  align-items: center;
`

const IconButton = styled.TouchableOpacity`
  border-width: 1px;
  border-color: #e0e0e0;
  padding: 14px;
  border-radius: 8px;
`

const Title = styled.Text`
  margin-top: 6px;
  font-size: 12px;
  color: ${theme.black};
`
