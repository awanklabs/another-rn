import React, { Component } from 'react'
import styled from 'styled-components'
import { Picker, ActivityIndicator, Text } from 'react-native'
import { connect } from 'react-redux'
import AntDesignIcons from 'react-native-vector-icons/AntDesign'
import { selectContactPhone } from 'react-native-select-contact'
import { Formik } from 'formik'
import { ToastStyles } from 'react-native-toaster'
// Constants
import { theme } from '../../../configs/constants'
// Actions
import {
  fetchProvider,
  fetchProducts,
  buyProduct
} from '../../../configs/redux/actions/epayment'
import { setToastMessage } from '../../../configs/redux/actions/global'
// Constants
import { epaymentTypeConstant } from '../../../configs/constants'
// Utils
import { formatPrice } from '../../../utils'
// Components
import Toast from '../../../components/molecules/Toast'

class EpaymentTelponSmsScreen extends Component {
  static navigationOptions = {
    title: 'Paket Telpon & SMS'
  }

  constructor(props) {
    super(props)
    this.state = {
      prefix: '',
      provider: '',
      products: [],
      selectedProduct: null,
      isLoading: false
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (this.state.prefix !== '' && prevState.prefix !== this.state.prefix) {
      await this.getProvider()
    }

    if (
      this.state.provider !== '' &&
      prevState.provider !== this.state.provider
    ) {
      await this.getProducts()
    }
  }

  getProvider = async () => {
    this.setState({ isLoading: true })
    const provider = await this.props.dispatch(fetchProvider(this.state.prefix))
    this.setState({ provider, isLoading: false })
  }

  getProducts = async () => {
    this.setState({ isLoading: true, products: [], selectedProduct: null })
    const result = await this.props.dispatch(
      fetchProducts(epaymentTypeConstant.PAKET_TELPON_SMS, this.state.provider)
    )
    if (result.status) {
      console.log(result.data)
      this.setState({ products: result.data })
      if (result.data.length > 0) {
        this.setState({ selectedProduct: result.data[0] })
      }
    }
    this.setState({ isLoading: false })
  }

  render() {
    const { isLoading, products, selectedProduct } = this.state

    return (
      <Container>
        <Toast />
        <Formik
          enableReinitialize
          initialValues={{
            code: '',
            destination: ''
          }}
          onSubmit={async (values, { setErrors, setSubmitting }) => {
            setSubmitting(true)
            const args = {
              code: selectedProduct.code,
              destination: values.destination
            }
            const result = await this.props.dispatch(buyProduct(args))
            if (result.status) {
              this.props.dispatch(
                setToastMessage({
                  text: result.message,
                  styles: ToastStyles.success
                })
              )
              this.props.navigation.navigate('Home')
            } else {
              result.errors && setErrors(result.errors)
              this.props.dispatch(
                setToastMessage({
                  text: result.message,
                  styles: ToastStyles.error
                })
              )
            }
            setSubmitting(false)
          }}>
          {({ values, errors, handleSubmit, isSubmitting, setFieldValue }) => {
            const getPhoneNumber = () => {
              return selectContactPhone().then(selection => {
                if (!selection) {
                  return null
                }
                let { selectedPhone } = selection
                let destination = selectedPhone.number
                destination = destination.replace(/\s/g, '')
                destination = destination.replace('+', '')
                destination = destination.replace(/-/g, '')
                if (destination.substring(0, 2) === '62') {
                  destination = destination.substring(2)
                  destination = `0${destination}`
                }
                setFieldValue('destination', destination)
                this.setState({ prefix: destination.substring(0, 4) })
                return selectedPhone.number
              })
            }

            const handleChangeDestination = value => {
              setFieldValue('destination', value)
              if (value.length < 4 && this.state.prefix !== '') {
                this.setState({ prefix: '' })
              } else if (
                value.length >= 4 &&
                value.substring(0, 4) !== this.state.prefix
              ) {
                this.setState({ prefix: value.substring(0, 4) })
              }
            }

            const handleChangeSelectedProduct = data => {
              this.setState({ selectedProduct: data })
            }

            return (
              <>
                <Row>
                  <Input
                    placeholder="Masukkan Nomor HP"
                    onChangeText={handleChangeDestination}
                    value={values.destination}
                    keyboardType="numeric"
                    hasError={errors.destination}
                  />
                  <ButtonContact onPress={getPhoneNumber}>
                    <AntDesignIcons name="contacts" color="white" size={20} />
                  </ButtonContact>
                </Row>
                <ErrorsView>
                  {errors.destination &&
                    errors.destination.map((error, index) => {
                      return (
                        <ErrorText key={index} numberOfLines={2}>
                          {error}
                        </ErrorText>
                      )
                    })}
                </ErrorsView>

                {isLoading && (
                  <ActivityIndicator
                    size="large"
                    color={theme.primary}
                    style={{ marginTop: 10 }}
                  />
                )}

                {products.length > 0 && (
                  <PickerWrapper>
                    <Picker
                      prompt="Daftar produk"
                      selectedValue={selectedProduct}
                      style={{
                        height: 50,
                        width: '100%'
                      }}
                      onValueChange={handleChangeSelectedProduct}>
                      <Picker.Item label="Silahkan pilih ..." value={null} />
                      {products.map((dt, index) => {
                        return (
                          <Picker.Item
                            key={index}
                            label={`${dt.name} - ${formatPrice(dt.sellPrice)}`}
                            value={dt}
                          />
                        )
                      })}
                    </Picker>
                  </PickerWrapper>
                )}

                {selectedProduct && (
                  <DetailCard>
                    <Row>
                      <WrapperLabel>
                        <Text>Produk</Text>
                      </WrapperLabel>
                      <Text>: </Text>
                      <WrapperValue>
                        <Text>{selectedProduct.name}</Text>
                      </WrapperValue>
                    </Row>
                    <Row>
                      <WrapperLabel>
                        <Text>Harga</Text>
                      </WrapperLabel>
                      <Text>: </Text>
                      <WrapperValue>
                        <Text>{formatPrice(selectedProduct.sellPrice)}</Text>
                      </WrapperValue>
                    </Row>
                  </DetailCard>
                )}

                <ButtonBuy
                  disabled={selectedProduct === null}
                  onPress={handleSubmit}>
                  {isSubmitting ? (
                    <ActivityIndicator size="small" color="white" />
                  ) : (
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        fontSize: 20
                      }}>
                      Bayar
                    </Text>
                  )}
                </ButtonBuy>
              </>
            )
          }}
        </Formik>
      </Container>
    )
  }
}

const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(
  null,
  mapDispatchToProps
)(EpaymentTelponSmsScreen)

const Container = styled.View`
  flex: 1;
  margin: 20px;
`

const Row = styled.View`
  flex-direction: row;
`

const Input = styled.TextInput`
  flex: 1;
  margin-right: 20px;
  padding-bottom: 0px;
  border-bottom-width: 1px;
  border-bottom-color: ${props =>
    props.hasError ? 'red' : `${theme.blackThin}`};
`

const ButtonContact = styled.TouchableOpacity`
  background-color: ${theme.primary};
  padding: 6px;
  border-radius: 4px;
  align-self: center;
`

const PickerWrapper = styled.View`
  border-bottom-width: 1px;
  border-color: ${theme.blackThin};
`

const DetailCard = styled.View`
  margin-top: 20px;
  border-width: 1px;
  border-color: ${theme.primary};
  padding: 10px;
  border-radius: 4px;
`

const WrapperLabel = styled.View`
  flex: 1;
`

const WrapperValue = styled.View`
  flex: 2;
`

const ButtonBuy = styled.TouchableOpacity`
  margin-top: 20px;
  background-color: ${theme.primary};
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  padding: 10px;
  border-radius: 4px;
`

const ErrorsView = styled.View``

const ErrorText = styled.Text`
  color: red;
`
