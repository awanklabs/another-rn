import React, { Component } from 'react'
import styled from 'styled-components'
import Swiper from 'react-native-swiper'
import { connect } from 'react-redux'
// Constants
import { theme } from '../../../configs/constants'
// Actions
import { setReadIntro } from '../../../configs/redux/actions/global'

class IntroductionScreen extends Component {
  componentDidMount() {
    if (this.props.Global.readIntro) {
      this.props.navigation.replace('Home')
    }
  }

  handleSkip = () => {
    this.props.dispatch(setReadIntro(true))
    this.props.navigation.replace('Home')
  }

  render() {
    return (
      <Container>
        <Content>
          <Swiper paginationStyle={{ marginBottom: 100 }}>
            <SlideItem>
              <Image
                resizeMode="cover"
                source={require('../../../assets/images/intro_mobile_app.png')}
              />
              <Description>Mudah</Description>
            </SlideItem>
            <SlideItem>
              <Image
                resizeMode="cover"
                source={require('../../../assets/images/intro_secure.png')}
              />
              <Description>Aman</Description>
            </SlideItem>
            <SlideItem>
              <Image
                resizeMode="cover"
                source={require('../../../assets/images/intro_confirmed.png')}
              />
              <Description>Lancar</Description>
            </SlideItem>
          </Swiper>
        </Content>
        <Navigation>
          <SkipButton onPress={this.handleSkip}>
            <SkipText>Lewati</SkipText>
          </SkipButton>
        </Navigation>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  Global: state.Global
})

const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IntroductionScreen)

const Container = styled.View`
  flex: 1;
  background-color: white;
`

const Content = styled.View`
  flex: 1;
`

const SlideItem = styled.View`
  margin-top: 40px;
  margin-horizontal: 20px;
  align-items: center;
  justify-content: center;
`

const Image = styled.Image`
  width: 100%;
  height: 85%;
`

const Description = styled.Text`
  font-size: 30px;
  color: ${theme.primary};
  font-weight: bold;
`

const Label = styled.Text``

const Navigation = styled.View`
  margin-vertical: 10px;
  margin-right: 20px;
  justify-content: center;
  align-items: flex-end;
`

const SkipButton = styled.TouchableOpacity`
  background-color: ${theme.primary};
  border-radius: 8px;
  padding-vertical: 8px;
  padding-horizontal: 16px;
`

const SkipText = styled.Text`
  font-size: 14px;
  color: white;
  font-weight: bold;
`
