import React, { Component } from 'react'
import { ScrollView, ActivityIndicator, Picker } from 'react-native'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Formik } from 'formik'
import { ToastStyles } from 'react-native-toaster'
// Constants
import { theme } from '../../../configs/constants'
// Actions
import {
  fetchListBank,
  createRequest,
  fetchList,
  setParams
} from '../../../configs/redux/actions/topup'
import { setToastMessage } from '../../../configs/redux/actions/global'
// Components
import Toast from '../../../components/molecules/Toast'

class TopupCreateScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Tambah Topup Saldo'
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      isLoading: false
    }
  }

  async componentDidMount() {
    await this.fetchListBank()
  }

  fetchListBank = async () => {
    this.setState({ isLoading: true })
    await this.props.dispatch(fetchListBank())
    this.setState({ isLoading: false })
  }

  render() {
    const { isLoading } = this.state
    const { listBank } = this.props.Topup
    return (
      <Container>
        <Toast />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Formik
            initialValues={{
              bank: '',
              requestAmount: '50000'
            }}
            onSubmit={async (values, { setErrors, setSubmitting }) => {
              setSubmitting(true)
              const result = await this.props.dispatch(createRequest(values))
              if (result.status) {
                this.props.dispatch(setParams({ page: 1 }))
                await this.props.dispatch(fetchList())
                this.props.dispatch(
                  setToastMessage({
                    text:
                      'Request topup berhasil dibuat, silahkan lakukan transfer',
                    styles: ToastStyles.success
                  })
                )
                setSubmitting(false)
                this.props.navigation.navigate('Topup')
              } else {
                this.props.dispatch(
                  setToastMessage({
                    text: 'Terjadi kesalahan',
                    styles: ToastStyles.error
                  })
                )
                setErrors(result.errors)
                setSubmitting(false)
              }
            }}>
            {({
              values,
              errors,
              handleChange,
              isSubmitting,
              setFieldValue,
              handleSubmit
            }) => {
              const handleChangeBank = value => {
                setFieldValue('bank', value)
              }

              return (
                <>
                  <FormGroup>
                    <Label>Pilih Bank</Label>
                    <Picker
                      prompt="Daftar Bank"
                      selectedValue={values.bank}
                      style={{
                        height: 50,
                        width: '100%'
                      }}
                      onValueChange={handleChangeBank}>
                      <Picker.Item label="Silahkan pilih ..." value={null} />
                      {listBank.map((dt, index) => {
                        return (
                          <Picker.Item
                            key={index}
                            label={dt.name}
                            value={dt.code}
                          />
                        )
                      })}
                    </Picker>
                    <ErrorsView>
                      {errors.bank &&
                        errors.bank.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <FormGroup>
                    <Label>Nominal</Label>
                    <TextInput
                      placeholder="Nominal"
                      keyboardType="numeric"
                      hasError={errors.requestAmount}
                      value={values.requestAmount}
                      onChangeText={handleChange('requestAmount')}
                    />
                    <ErrorsView>
                      {errors.requestAmount &&
                        errors.requestAmount.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <SubmitButton
                    disabled={isSubmitting}
                    onPress={() => handleSubmit()}>
                    {isSubmitting ? (
                      <ActivityIndicator size="small" color="white" />
                    ) : (
                      <ButtonText>Tambah</ButtonText>
                    )}
                  </SubmitButton>
                </>
              )
            }}
          </Formik>

          {isLoading && (
            <ActivityIndicator size="large" color={theme.primary} />
          )}
        </ScrollView>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  Topup: state.Topup
})

const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TopupCreateScreen)

const Container = styled.View`
  flex: 1;
  /* background-color: white; */
  padding: 20px;
`

const FormGroup = styled.View`
  margin-bottom: 16px;
`

const Label = styled.Text`
  font-size: 14px;
  color: ${theme.black};
  font-weight: bold;
`

const PickerWrapper = styled.View`
  border-bottom-width: 1px;
  border-color: ${theme.blackThin};
`

const TextInput = styled.TextInput`
  padding-bottom: 0px;
  border-bottom-width: 1px;
  border-bottom-color: ${props =>
    props.hasError ? 'red' : `${theme.blackThin}`};
`

const ErrorsView = styled.View`
  margin-top: 8px;
`

const ErrorText = styled.Text`
  color: red;
`

const SubmitButton = styled.TouchableOpacity`
  padding-vertical: 14px;
  background-color: ${theme.primary};
  justify-content: center;
  align-items: center;
  border-radius: 8px;
`

const ButtonText = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: white;
`
