import React, { Component } from 'react'
import { TouchableOpacity, Text } from 'react-native'
import styled from 'styled-components'
// import { HeaderBackButton } from 'react-navigation-stack'
// Constants
import { theme } from '../../../configs/constants'
// Organisms
import DataList from '../../organisms/topup/DataList'
// Components
import Toast from '../../../components/molecules/Toast'

class TopupScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Topup Saldo',
      headerRight: (
        <TouchableOpacity
          onPress={() => navigation.navigate('TopupCreate')}
          style={{
            marginRight: 20,
            borderWidth: 1,
            borderColor: theme.primary,
            padding: 8,
            borderRadius: 4
          }}>
          <Text style={{ fontSize: 14, color: theme.primary }}>Tambah</Text>
        </TouchableOpacity>
      )
    }
  }

  render() {
    return (
      <Container>
        <Toast />
        <DataList />
      </Container>
    )
  }
}

export default TopupScreen

const Container = styled.View`
  flex: 1;
  /* background-color: ${theme.background}; */
`
