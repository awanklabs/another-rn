import React, { Component } from 'react'
import styled from 'styled-components'
import { ScrollView } from 'react-native'
import { connect } from 'react-redux'
// Actions
import { logout } from '../../../configs/redux/actions/auth'
import { setToastMessage } from '../../../configs/redux/actions/global'
import { ToastStyles } from 'react-native-toaster'
// Components
import ProfileHeader from '../../../components/molecules/profile/ProfileHeader'
// Constants
import { theme } from '../../../configs/constants'

class ProfileScreen extends Component {
  static navigationOptions = {
    header: null
  }

  handleBack = () => {
    this.props.navigation.goBack(null)
  }

  handleLogout = async () => {
    await this.props.dispatch(logout())
    this.props.dispatch(
      setToastMessage({
        text: 'Anda berhasil keluar',
        styles: ToastStyles.success
      })
    )
    this.props.navigation.navigate('Auth')
  }

  render() {
    const { profile } = this.props
    if (profile === null) {
      return <Container />
    }
    return (
      <Container>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ProfileHeader
            profile={profile}
            handleBack={this.handleBack}
            handleLogout={this.handleLogout}
          />

          <ContentMenu>
            <ItemMenu
              onPress={() =>
                this.props.navigation.navigate('EpaymentTransaction')
              }>
              <MenuText>Riwayat Transaksi</MenuText>
            </ItemMenu>

            <ItemMenu onPress={() => this.props.navigation.navigate('Topup')}>
              <MenuText>Riwayat Topup</MenuText>
            </ItemMenu>

            <ItemMenu>
              <MenuText>Edit Profil</MenuText>
            </ItemMenu>
          </ContentMenu>
        </ScrollView>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  profile: state.Auth.profile
})

const mapDispatchToprops = dispatch => ({ dispatch })

export default connect(
  mapStateToProps,
  mapDispatchToprops
)(ProfileScreen)

const Container = styled.View`
  flex: 1;
`

const ContentMenu = styled.View`
  padding-vertical: 10px;
`

const ItemMenu = styled.TouchableOpacity`
  margin-bottom: 10px;
  border-bottom-width: 1px;
  border-color: ${theme.blackSuperThin};
  padding-vertical: 10px;
`

const MenuText = styled.Text`
  color: ${theme.black};
  font-size: 16px;
  font-weight: bold;
  margin-left: 20px;
`
