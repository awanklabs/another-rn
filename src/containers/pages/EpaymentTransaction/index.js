import React, { Component } from 'react'
import styled from 'styled-components'
// Organisms
import DataList from '../../organisms/epayment-transaction/DataList'

class EpaymentTransactionScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Riwayat Transaksi'
    }
  }

  render() {
    return (
      <Container>
        <DataList />
      </Container>
    )
  }
}

export default EpaymentTransactionScreen

const Container = styled.View`
  flex: 1;
`
