import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { TouchableOpacity, ActivityIndicator, ScrollView } from 'react-native'
import { HeaderBackButton } from 'react-navigation-stack'
import { Formik } from 'formik'
// Constants
import { theme } from '../../../configs/constants'
// Actions
import { register } from '../../../configs/redux/actions/auth'
// Components
import Toast from '../../../components/molecules/Toast'

class RegisterScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      // title: 'Login',
      headerLeft: (
        <HeaderBackButton
          tintColor={theme.primary}
          onPress={() => navigation.goBack(null)}
        />
      )
    }
  }

  render() {
    return (
      <Container>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Toast />
          <Title>Daftar</Title>

          <Formik
            initialValues={{
              name: '',
              phone: '',
              email: '',
              password: '',
              passwordConfirmation: ''
            }}
            onSubmit={async (values, { setErrors, setSubmitting }) => {
              setSubmitting(true)
              const result = await this.props.dispatch(register(values))
              console.log(result)
              setSubmitting(false)
              if (result.status) {
                if (this.props.readIntro) {
                  this.props.navigation.navigate('App')
                } else {
                  this.props.navigation.navigate('Introduction')
                }
              } else {
                setErrors(result.errors)
              }
            }}>
            {({ values, errors, handleSubmit, handleChange, isSubmitting }) => {
              return (
                <>
                  <FormGroup>
                    <Label>NAMA</Label>
                    <TextInput
                      value={values.name}
                      onChangeText={handleChange('name')}
                      hasError={errors.name}
                    />
                    <ErrorsView>
                      {errors.name &&
                        errors.name.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <FormGroup>
                    <Label>NOMOR HANDPHONE</Label>
                    <TextInput
                      value={values.phone}
                      keyboardType="numeric"
                      onChangeText={handleChange('phone')}
                      hasError={errors.phone}
                    />
                    <ErrorsView>
                      {errors.phone &&
                        errors.phone.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <FormGroup>
                    <Label>EMAIL</Label>
                    <TextInput
                      value={values.email}
                      onChangeText={handleChange('email')}
                      hasError={errors.email}
                    />
                    <ErrorsView>
                      {errors.email &&
                        errors.email.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <FormGroup>
                    <Label>PASSWORD</Label>
                    <TextInput
                      secureTextEntry={true}
                      name="password"
                      value={values.password}
                      onChangeText={handleChange('password')}
                      hasError={errors.password}
                    />
                    <ErrorsView>
                      {errors.password &&
                        errors.password.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <FormGroup>
                    <Label>KONFIRMASI PASSWORD</Label>
                    <TextInput
                      secureTextEntry={true}
                      name="passwordConfirmation"
                      value={values.passwordConfirmation}
                      onChangeText={handleChange('passwordConfirmation')}
                      hasError={errors.passwordConfirmation}
                    />
                    <ErrorsView>
                      {errors.passwordConfirmation &&
                        errors.passwordConfirmation.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <SubmitButton
                    disabled={isSubmitting}
                    onPress={() => handleSubmit()}>
                    {isSubmitting ? (
                      <ActivityIndicator size="small" color="white" />
                    ) : (
                      <ButtonText>Daftar</ButtonText>
                    )}
                  </SubmitButton>
                </>
              )
            }}
          </Formik>

          <WrapperFooterLink>
            <IntroText>Sudah punya akun? </IntroText>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Login')}>
              <LinkText>Login</LinkText>
            </TouchableOpacity>
          </WrapperFooterLink>
        </ScrollView>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  Auth: state.Auth
})

const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterScreen)

const Container = styled.View`
  flex: 1;
  align-content: center;
  justify-content: center;
  padding: 24px;
`

const Title = styled.Text`
  font-size: 22px;
  color: ${theme.black};
  margin-bottom: 38px;
`

const FormGroup = styled.View`
  margin-bottom: 38px;
`

const Label = styled.Text`
  font-size: 11px;
  color: ${theme.black};
`

const TextInput = styled.TextInput`
  padding-bottom: 0px;
  border-bottom-width: 1px;
  border-bottom-color: ${props =>
    props.hasError ? 'red' : `${theme.blackThin}`};
`

const ErrorsView = styled.View``

const ErrorText = styled.Text`
  color: red;
`

const SubmitButton = styled.TouchableOpacity`
  padding-vertical: 14px;
  background-color: ${theme.primary};
  justify-content: center;
  align-items: center;
  border-radius: 8px;
`

const ButtonText = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: white;
`

const WrapperFooterLink = styled.View`
  margin-top: 14px;
  flex-direction: row;
  justify-content: center;
`

const IntroText = styled.Text`
  color: ${theme.blackThin};
  font-size: 14px;
`

const LinkText = styled.Text`
  color: ${theme.primary};
  font-weight: bold;
  font-size: 14px;
`
