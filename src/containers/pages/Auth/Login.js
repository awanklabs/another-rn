import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { TouchableOpacity, ActivityIndicator, ScrollView } from 'react-native'
import { HeaderBackButton } from 'react-navigation-stack'
import { Formik } from 'formik'
// Constants
import { theme } from '../../../configs/constants'
// Actions
import { login } from '../../../configs/redux/actions/auth'
// Components
import Toast from '../../../components/molecules/Toast'

class LoginScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      // title: 'Login',
      headerLeft: (
        <HeaderBackButton
          tintColor={theme.primary}
          onPress={() => navigation.goBack(null)}
        />
      )
    }
  }

  componentDidMount() {
    if (this.props.Auth.profile !== null) {
      this.props.navigation.navigate('App')
    }
  }

  render() {
    return (
      <Container>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Toast />
          <Title>Login</Title>

          <Formik
            initialValues={{
              email: '',
              password: ''
            }}
            onSubmit={async (values, { setErrors, setSubmitting }) => {
              setSubmitting(true)
              const result = await this.props.dispatch(login(values))
              setSubmitting(false)
              if (result.status) {
                if (this.props.readIntro) {
                  this.props.navigation.navigate('App')
                } else {
                  this.props.navigation.navigate('Introduction')
                }
              } else {
                setErrors(result.errors)
              }
            }}>
            {({ values, errors, handleSubmit, handleChange, isSubmitting }) => {
              return (
                <>
                  <FormGroup>
                    <Label numberOfLines={1}>EMAIL / NOMOR HANDPHONE</Label>
                    <TextInput
                      name="email"
                      value={values.email}
                      onChangeText={handleChange('email')}
                      hasError={errors.email}
                    />
                    <ErrorsView>
                      {errors.email &&
                        errors.email.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <FormGroup>
                    <Label>PASSWORD</Label>
                    <TextInput
                      secureTextEntry={true}
                      name="password"
                      value={values.password}
                      onChangeText={handleChange('password')}
                      hasError={errors.password}
                    />
                    <ErrorsView>
                      {errors.password &&
                        errors.password.map((error, index) => {
                          return (
                            <ErrorText key={index} numberOfLines={2}>
                              {error}
                            </ErrorText>
                          )
                        })}
                    </ErrorsView>
                  </FormGroup>

                  <SubmitButton
                    disabled={isSubmitting}
                    onPress={() => handleSubmit()}>
                    {isSubmitting ? (
                      <ActivityIndicator size="small" color="white" />
                    ) : (
                      <ButtonText>Login</ButtonText>
                    )}
                  </SubmitButton>
                </>
              )
            }}
          </Formik>

          <WrapperFooterLink>
            <IntroText>Belum punya akun? </IntroText>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Register')}>
              <LinkText>Daftar sekarang</LinkText>
            </TouchableOpacity>
          </WrapperFooterLink>

          <WrapperFooterLink>
            <LinkText>Lupa password?</LinkText>
          </WrapperFooterLink>
        </ScrollView>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  Auth: state.Auth
})

const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen)

const Container = styled.View`
  flex: 1;
  align-content: center;
  justify-content: center;
  margin: 24px;
`

const Title = styled.Text`
  font-size: 22px;
  color: ${theme.black};
  margin-bottom: 38px;
`

const FormGroup = styled.View`
  margin-bottom: 38px;
`

const Label = styled.Text`
  font-size: 11px;
  color: ${theme.black};
  max-width: 100%;
`

const TextInput = styled.TextInput`
  padding-bottom: 0px;
  border-bottom-width: 1px;
  border-bottom-color: ${props =>
    props.hasError ? 'red' : `${theme.blackThin}`};
`

const ErrorsView = styled.View``

const ErrorText = styled.Text`
  color: red;
`

const SubmitButton = styled.TouchableOpacity`
  padding-vertical: 14px;
  background-color: ${theme.primary};
  justify-content: center;
  align-items: center;
  border-radius: 8px;
`

const ButtonText = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: white;
`

const WrapperFooterLink = styled.View`
  margin-top: 14px;
  flex-direction: row;
  justify-content: center;
`

const IntroText = styled.Text`
  color: ${theme.blackThin};
  font-size: 14px;
`

const LinkText = styled.Text`
  color: ${theme.primary};
  font-weight: bold;
  font-size: 14px;
`
