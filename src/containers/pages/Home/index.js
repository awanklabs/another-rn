import React, { Component } from 'react'
import {
  Text,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  PermissionsAndroid,
  Platform
} from 'react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome'
import { ToastStyles } from 'react-native-toaster'
// Constants
import { theme } from '../../../configs/constants'
// Organisms
import HomeWallet from '../../organisms/home/HomeWallet'
import HomeEpayment from '../../organisms/home/HomeEpayment'
// Actions
import { fetchProfile, logout } from '../../../configs/redux/actions/auth'
import { setToastMessage } from '../../../configs/redux/actions/global'
// Components
import Toast from '../../../components/molecules/Toast'

class HomeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false
    }
  }

  async componentDidMount() {
    await this.initPermission()
    await this.fetchProfile()
  }

  initPermission = async () => {
    if (Platform.OS === 'android') {
      const contactPermission = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS
      )
      if (contactPermission !== PermissionsAndroid.RESULTS.GRANTED) {
        await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
          {
            title: 'MyBitniaga needs Contact Permission',
            message: 'MyBitniaga needs access to your contact',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK'
          }
        )
      }
    }
  }

  fetchProfile = async () => {
    this.setState({ isLoading: true })
    const result = await this.props.dispatch(fetchProfile())
    if (result.status === 403) {
      this.props.dispatch(
        setToastMessage({
          text: result.message,
          styles: ToastStyles.error
        })
      )
      this.props.dispatch(logout())
      this.setState({ isLoading: false })
      this.props.navigation.navigate('Auth')
    } else {
      this.setState({ isLoading: false })
    }
  }

  render() {
    const { profile } = this.props.Auth
    const { isLoading } = this.state
    return (
      <Container>
        <Toast />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Header>
            <Title>My Bitniaga</Title>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Profile')}>
              <FontAwesomeIcons
                name="user-circle"
                size={25}
                color={theme.primary}
              />
            </TouchableOpacity>
          </Header>

          <HomeWallet
            profile={profile}
            handleRefresh={this.fetchProfile}
            loading={isLoading}
          />

          <SectionTitle>E Payment</SectionTitle>
          <HomeEpayment />
        </ScrollView>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  Auth: state.Auth
})

const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen)

const Container = styled.SafeAreaView`
  flex: 1;
`

const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  height: 60px;
  padding-vertical: 10px;
  padding-horizontal: 20px;
  border-bottom-width: 1px;
  border-color: ${theme.blackThin};
`

const Title = styled.Text`
  color: ${theme.primary};
  font-size: 20px;
  font-weight: bold;
`

const SectionTitle = styled.Text`
  margin-left: 20px;
  font-size: 16px;
  color: #828282;
  font-weight: bold;
`

// const Text = styled.Text``
