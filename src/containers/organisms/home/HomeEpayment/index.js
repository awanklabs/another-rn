import React from 'react'
import styled from 'styled-components'
import { withNavigation } from 'react-navigation'

// Constants
import {
  theme,
  IconPulsa,
  IconPaketData,
  IconTelpon,
  IconPulsaPasca,
  IconPLN,
  IconAsuransi,
  IconAngsuran,
  IconPDAM,
  IconLainnya,
  IconGojek
} from '../../../../configs/constants'

const HomeEpayment = props => {
  return (
    <Container>
      <EpyamentItem>
        <IconButton onPress={() => props.navigation.navigate('EpaymentPulsa')}>
          <IconPulsa size={40} />
        </IconButton>
        <Title>Pulsa</Title>
      </EpyamentItem>
      <EpyamentItem>
        <IconButton
          onPress={() => props.navigation.navigate('EpaymentPaketData')}>
          <IconPaketData size={40} />
        </IconButton>
        <Title>Paket Data</Title>
      </EpyamentItem>
      <EpyamentItem>
        <IconButton
          onPress={() => props.navigation.navigate('EpaymentTelponSms')}>
          <IconTelpon size={40} />
        </IconButton>
        <Title>Telpon & SMS</Title>
      </EpyamentItem>
      <EpyamentItem>
        <IconButton onPress={() => props.navigation.navigate('EpaymentPln')}>
          <IconPLN size={40} />
        </IconButton>
        <Title>PLN</Title>
      </EpyamentItem>
      <EpyamentItem>
        <IconButton
          onPress={() => props.navigation.navigate('EpaymentInsurance')}>
          <IconAsuransi size={40} />
        </IconButton>
        <Title>Asuransi</Title>
      </EpyamentItem>
      <EpyamentItem>
        <IconButton
          onPress={() => props.navigation.navigate('EpaymentFinance')}>
          <IconAngsuran size={40} />
        </IconButton>
        <Title>Finance</Title>
      </EpyamentItem>
      <EpyamentItem>
        <IconButton onPress={() => props.navigation.navigate('EpaymentPdam')}>
          <IconPDAM size={40} />
        </IconButton>
        <Title>PDAM</Title>
      </EpyamentItem>
      <EpyamentItem>
        <IconButton onPress={() => props.navigation.navigate('EpaymentGopay')}>
          <IconGojek size={40} />
        </IconButton>
        <Title>GO-PAY</Title>
      </EpyamentItem>
      <EpyamentItem>
        <IconButton onPress={() => props.navigation.navigate('Epayment')}>
          <IconLainnya size={40} />
        </IconButton>
        <Title>Lainnya</Title>
      </EpyamentItem>
    </Container>
  )
}

export default withNavigation(HomeEpayment)

const Container = styled.View`
  margin-top: 10px;
  margin-horizontal: 20px;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`

const EpyamentItem = styled.View`
  margin-bottom: 14px;
  width: 30%;
  justify-content: center;
  align-items: center;
`

const IconButton = styled.TouchableOpacity`
  border-width: 1px;
  border-color: #e0e0e0;
  padding: 10px;
  border-radius: 8px;
`

const Title = styled.Text`
  margin-top: 6px;
  font-size: 12px;
  color: ${theme.black};
`
