import React from 'react'
import styled from 'styled-components'
import { TouchableOpacity, ActivityIndicator } from 'react-native'
import { withNavigation } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
// Constants
import { theme } from '../../../../configs/constants'
// Utils
import { formatPrice } from '../../../../utils'

const HomeWallet = props => {
  const { profile, loading } = props

  if (loading || profile === null) {
    return (
      <Container>
        <ActivityIndicator size="large" color="white" />
      </Container>
    )
  }

  return (
    <Container>
      <Label>Saldo Anda</Label>
      <BalanceText>{formatPrice(profile.balance)}</BalanceText>
      <DetailWrapper>
        <NameWrapper>
          <LabelName>Nama:</LabelName>
          <Name>{profile.name}</Name>
        </NameWrapper>
        <TouchableOpacity onPress={() => props.handleRefresh()}>
          <MaterialIcons name="refresh" size={30} color="white" />
        </TouchableOpacity>
        <TopupButton onPress={() => props.navigation.navigate('Topup')}>
          <TopupText>TOP UP</TopupText>
        </TopupButton>
      </DetailWrapper>
    </Container>
  )
}

export default withNavigation(HomeWallet)

const Container = styled.View`
  margin: 20px;
  padding: 20px;
  border-radius: 8px;
  background-color: ${theme.primary};
`

const Label = styled.Text`
  font-size: 16px;
  color: white;
  font-weight: bold;
`

const BalanceText = styled.Text`
  margin-top: 20px;
  font-size: 24px;
  color: white;
  font-weight: bold;
`

const DetailWrapper = styled.View`
  margin-top: 20px;
  flex-direction: row;
  align-items: center;
`

const NameWrapper = styled.View`
  flex: 1;
`

const LabelName = styled.Text`
  font-size: 14px;
  color: white;
  width: 80%;
`

const Name = styled.Text`
  font-size: 16px;
  color: white;
  font-weight: bold;
  width: 80%;
`

const TopupButton = styled.TouchableOpacity`
  margin-left: 16px;
  border-width: 2px;
  border-color: white;
  border-radius: 4px;
  padding-vertical: 6px;
  padding-horizontal: 12px;
`

const TopupText = styled.Text`
  font-size: 16px;
  color: white;
`
