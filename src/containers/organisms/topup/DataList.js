import React, { Component } from 'react'
import {
  ScrollView,
  RefreshControl,
  FlatList,
  ActivityIndicator,
  Text
} from 'react-native'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { ToastStyles } from 'react-native-toaster'
// Actions
import { setToastMessage } from '../../../configs/redux/actions/global'
import {
  setParams,
  fetchList,
  fetchNextList,
  cancelRequest
} from '../../../configs/redux/actions/topup'
// Molecules
import CardItem from '../../../components/molecules/topup/CardItem'
// Constants
import { theme } from '../../../configs/constants'

class DataList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      refreshing: false,
      dummy: 0
    }
  }

  async componentDidMount() {
    await this.fetchData()
  }

  fetchData = async () => {
    console.log('fired')
    const params = {
      page: 1
    }

    this.setState({ refreshing: true, dummy: this.state.dummy + 1 })
    this.props.dispatch(setParams(params))
    await this.props.dispatch(fetchList())
    this.setState({ refreshing: false })
  }

  fetchNextData = async () => {
    const state = this.props.Topup
    const params = {
      page: state.page + 1
    }

    this.setState({ refreshing: true, dummy: this.state.dummy + 1 })
    this.props.dispatch(setParams(params))
    await this.props.dispatch(fetchNextList())
    this.setState({ refreshing: false })
  }

  cancelRequest = async id => {
    this.setState({ refreshing: true })
    const result = await this.props.dispatch(cancelRequest(id))
    if (result.status) {
      await this.fetchData()
    }
    this.setState({ refreshing: false })
    this.props.dispatch(
      setToastMessage({
        text: 'Topup berhasil dibatalkan',
        styles: ToastStyles.success
      })
    )
  }

  render() {
    const { page, pages, data } = this.props.Topup
    const { refreshing } = this.state

    if (data.length === 0) {
      return (
        <Container>
          <Text>Tidak ada data</Text>
        </Container>
      )
    }
    return (
      <>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.fetchData}
            />
          }
          showsVerticalScrollIndicator={false}
          style={{ margin: 16 }}>
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <CardItem
                item={item}
                handleCancel={this.cancelRequest}
                isLoading={refreshing}
              />
            )}
            refreshing={refreshing}
            onRefresh={this.fetchData}
            keyExtractor={item => item._id}
            extraData={this.state.dummy}
          />

          {page < pages && (
            <LoadMoreButton onPress={this.fetchNextData}>
              {refreshing ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <ButtonText>Tampilkan lebih</ButtonText>
              )}
            </LoadMoreButton>
          )}
        </ScrollView>
      </>
    )
  }
}

const mapStateToProps = state => ({
  Topup: state.Topup
})

const mapDispatchToProps = dispatch => ({ dispatch })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DataList)

const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`

const LoadMoreButton = styled.TouchableOpacity`
  width: 200px;
  background-color: ${theme.primary};
  border-radius: 8px;
  padding: 8px;
  align-self: center;
  align-items: center;
`

const ButtonText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
  color: white;
`
