import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import store, { persistor } from './src/configs/redux/store'
import { StatusBar } from 'react-native'
import AppContainer from './src/navigations'
import { theme } from './src/configs/constants'
// Utils
import { checkPermission } from './src/utils'

class App extends Component {
  async componentDidMount() {
    await checkPermission()
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar backgroundColor={theme.primary} barStyle="light-content" />
          <AppContainer />
        </PersistGate>
      </Provider>
    )
  }
}

export default App
